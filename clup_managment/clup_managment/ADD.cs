﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class ADDFilmAndSeries : Form
    {
        public ADDFilmAndSeries()
        {
            InitializeComponent();
        }

        private void ADDFilmAndSeries_Load(object sender, EventArgs e)
        {
            Int32 num = 0;

            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            this.CenterToScreen();
            num = Convert.ToInt32(seriesTableAdapter.ScalarQuerymaxcode());
            num++;
            scode.Text = num.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                filmTableAdapter.Insert(int.Parse(scode.Text), sname.Text, sdate.Text, textBox1.Text, int.Parse(Sprice.Text));
                seriesTableAdapter.Insert(int.Parse(scode.Text), sname.Text, sdate.Text, sganr.Text, int.Parse(snumOfFasl.Text), int.Parse(Sprice.Text), int.Parse(spart.Text), int.Parse(sAllEpizode.Text), sparttopart.Text);

                MessageBox.Show("ویدئو با موفقیت ثبت شد");
                this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);

            }
            catch (Exception ex)
            {
                //display error message
                MessageBox.Show("Exception: " + ex.Message);
                MessageBox.Show("خطا در ثبت سریال");
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName ;
            }
         
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (sname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

                try
                {
                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, '%' + sname.Text + '%');
                    filmTableAdapter.FillByname(clup_DBDataSet.film, '%' + sname.Text + '%');


                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - ویدئو یافت نشد");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
              
                seriesTableAdapter.UpdateQueryupdatebycode(sname.Text, sdate.Text, sganr.Text, int.Parse(snumOfFasl.Text), int.Parse(Sprice.Text), int.Parse(spart.Text), int.Parse(sAllEpizode.Text), sparttopart.Text, int.Parse(scode2.Text));
                filmTableAdapter.UpdateQuerybyCode(textBox1.Text, int.Parse(scode2.Text));
                this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
                this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);

            }
            catch (Exception ex)
            {
                //display error message
                MessageBox.Show("خطا در ورود - ویدئو یافت نشد");
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
        }
    }
}
