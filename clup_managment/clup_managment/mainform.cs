﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class mainform : Form
    {
        public mainform()
        {
            InitializeComponent();
        }

        private void لیستفیلمهاوسریالهاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form filmAndSeries = new filmAndSeries();
            filmAndSeries.ShowDialog();
        }

        private void mainform_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            Form Form1 = new Form1();
            Form1.ShowDialog();
        }

        private void b2_Click(object sender, EventArgs e)
        {
            Form ADDFilmAndSeries = new ADDFilmAndSeries();
            ADDFilmAndSeries.ShowDialog();
        }

        private void b3_Click(object sender, EventArgs e)
        {
            if (Class1.userType=="0"){
            Form deleteFilmAndSeries = new deleteFilmAndSeries();
            deleteFilmAndSeries.ShowDialog();
                }
            else{
                MessageBox.Show("شما به این قسمت دسترسی ندارید");
            }
        }

        private void u1_Click(object sender, EventArgs e)
        {
            if (Class1.userType=="0"){
            Form user = new user();
            user.ShowDialog();
            }
            else{
                MessageBox.Show("شما به این قسمت دسترسی ندارید");
            }
        }

        private void b4_Click(object sender, EventArgs e)
        {
            Form rent = new rent();
            rent.ShowDialog();

        }
    }
}
