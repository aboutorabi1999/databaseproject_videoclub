﻿namespace clup_managment
{
    partial class rent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clup_DBDataSet = new clup_managment.clup_DBDataSet();
            this.rentTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.rentTableAdapter();
            this.filmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.filmTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.filmTableAdapter();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.seriescodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesgenreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesSeasonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentpriceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesTotalPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesRengPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.seriesTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.SeriesTableAdapter();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.UsersTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.usercode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.filmcode = new System.Windows.Forms.TextBox();
            this.emtiaz = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nazar = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Price = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.code1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rentBindingSource
            // 
            this.rentBindingSource.DataMember = "rent";
            this.rentBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // clup_DBDataSet
            // 
            this.clup_DBDataSet.DataSetName = "clup_DBDataSet";
            this.clup_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rentTableAdapter
            // 
            this.rentTableAdapter.ClearBeforeFill = true;
            // 
            // filmBindingSource
            // 
            this.filmBindingSource.DataMember = "film";
            this.filmBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // filmTableAdapter
            // 
            this.filmTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.seriescodeDataGridViewTextBoxColumn,
            this.seriesnameDataGridViewTextBoxColumn,
            this.seriesdateDataGridViewTextBoxColumn,
            this.seriesgenreDataGridViewTextBoxColumn,
            this.seriesSeasonDataGridViewTextBoxColumn,
            this.rentpriceDataGridViewTextBoxColumn1,
            this.seriesPartDataGridViewTextBoxColumn,
            this.seriesTotalPartDataGridViewTextBoxColumn,
            this.seriesRengPartDataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.seriesBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(12, 17);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(543, 306);
            this.dataGridView3.TabIndex = 2;
            // 
            // seriescodeDataGridViewTextBoxColumn
            // 
            this.seriescodeDataGridViewTextBoxColumn.DataPropertyName = "Series_code";
            this.seriescodeDataGridViewTextBoxColumn.HeaderText = "کد ویدئو";
            this.seriescodeDataGridViewTextBoxColumn.Name = "seriescodeDataGridViewTextBoxColumn";
            // 
            // seriesnameDataGridViewTextBoxColumn
            // 
            this.seriesnameDataGridViewTextBoxColumn.DataPropertyName = "Series_name";
            this.seriesnameDataGridViewTextBoxColumn.HeaderText = "نام فیلم یا سریال";
            this.seriesnameDataGridViewTextBoxColumn.Name = "seriesnameDataGridViewTextBoxColumn";
            // 
            // seriesdateDataGridViewTextBoxColumn
            // 
            this.seriesdateDataGridViewTextBoxColumn.DataPropertyName = "Series_date";
            this.seriesdateDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.seriesdateDataGridViewTextBoxColumn.Name = "seriesdateDataGridViewTextBoxColumn";
            // 
            // seriesgenreDataGridViewTextBoxColumn
            // 
            this.seriesgenreDataGridViewTextBoxColumn.DataPropertyName = "Series_genre";
            this.seriesgenreDataGridViewTextBoxColumn.HeaderText = "ژانر";
            this.seriesgenreDataGridViewTextBoxColumn.Name = "seriesgenreDataGridViewTextBoxColumn";
            // 
            // seriesSeasonDataGridViewTextBoxColumn
            // 
            this.seriesSeasonDataGridViewTextBoxColumn.DataPropertyName = "Series_Season";
            this.seriesSeasonDataGridViewTextBoxColumn.HeaderText = "فصل";
            this.seriesSeasonDataGridViewTextBoxColumn.Name = "seriesSeasonDataGridViewTextBoxColumn";
            // 
            // rentpriceDataGridViewTextBoxColumn1
            // 
            this.rentpriceDataGridViewTextBoxColumn1.DataPropertyName = "rent_price";
            this.rentpriceDataGridViewTextBoxColumn1.HeaderText = "قیمت کرایه";
            this.rentpriceDataGridViewTextBoxColumn1.Name = "rentpriceDataGridViewTextBoxColumn1";
            // 
            // seriesPartDataGridViewTextBoxColumn
            // 
            this.seriesPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Part";
            this.seriesPartDataGridViewTextBoxColumn.HeaderText = "قسمت";
            this.seriesPartDataGridViewTextBoxColumn.Name = "seriesPartDataGridViewTextBoxColumn";
            // 
            // seriesTotalPartDataGridViewTextBoxColumn
            // 
            this.seriesTotalPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Total_Part";
            this.seriesTotalPartDataGridViewTextBoxColumn.HeaderText = "تعداد قسمت ها";
            this.seriesTotalPartDataGridViewTextBoxColumn.Name = "seriesTotalPartDataGridViewTextBoxColumn";
            // 
            // seriesRengPartDataGridViewTextBoxColumn
            // 
            this.seriesRengPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Reng_Part";
            this.seriesRengPartDataGridViewTextBoxColumn.HeaderText = "قسمت از .... تا ....";
            this.seriesRengPartDataGridViewTextBoxColumn.Name = "seriesRengPartDataGridViewTextBoxColumn";
            // 
            // seriesBindingSource
            // 
            this.seriesBindingSource.DataMember = "Series";
            this.seriesBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // seriesTableAdapter
            // 
            this.seriesTableAdapter.ClearBeforeFill = true;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(861, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 35);
            this.label1.TabIndex = 17;
            this.label1.Text = "کد کاربر";
            // 
            // usercode
            // 
            this.usercode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_code", true));
            this.usercode.Enabled = false;
            this.usercode.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.usercode.Location = new System.Drawing.Point(577, 63);
            this.usercode.Name = "usercode";
            this.usercode.Size = new System.Drawing.Size(277, 40);
            this.usercode.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(861, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 35);
            this.label2.TabIndex = 19;
            this.label2.Text = "کد فیلم یا سریال";
            // 
            // filmcode
            // 
            this.filmcode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_code", true));
            this.filmcode.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.filmcode.Location = new System.Drawing.Point(577, 109);
            this.filmcode.Name = "filmcode";
            this.filmcode.Size = new System.Drawing.Size(278, 40);
            this.filmcode.TabIndex = 18;
            // 
            // emtiaz
            // 
            this.emtiaz.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.emtiaz.Location = new System.Drawing.Point(577, 155);
            this.emtiaz.Name = "emtiaz";
            this.emtiaz.Size = new System.Drawing.Size(277, 40);
            this.emtiaz.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(861, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 35);
            this.label4.TabIndex = 23;
            this.label4.Text = "امتیاز";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(861, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 35);
            this.label5.TabIndex = 25;
            this.label5.Text = "نظر";
            // 
            // nazar
            // 
            this.nazar.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.nazar.Location = new System.Drawing.Point(577, 201);
            this.nazar.Name = "nazar";
            this.nazar.Size = new System.Drawing.Size(277, 40);
            this.nazar.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(861, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 35);
            this.label6.TabIndex = 27;
            this.label6.Text = "قیمت";
            // 
            // Price
            // 
            this.Price.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "rent_price", true));
            this.Price.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Price.Location = new System.Drawing.Point(577, 247);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(277, 40);
            this.Price.TabIndex = 26;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(577, 293);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(403, 42);
            this.button1.TabIndex = 28;
            this.button1.Text = "ثبت";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(862, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 35);
            this.label3.TabIndex = 30;
            this.label3.Text = "کد کرایه";
            // 
            // code1
            // 
            this.code1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rentBindingSource, "rent_code", true));
            this.code1.Enabled = false;
            this.code1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.code1.Location = new System.Drawing.Point(578, 17);
            this.code1.Name = "code1";
            this.code1.Size = new System.Drawing.Size(277, 40);
            this.code1.TabIndex = 29;
            // 
            // rent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 355);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.code1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nazar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.emtiaz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.filmcode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.usercode);
            this.Controls.Add(this.dataGridView3);
            this.Name = "rent";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "کرایه فیلم";
            this.Load += new System.EventHandler(this.rent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private clup_DBDataSet clup_DBDataSet;
        private System.Windows.Forms.BindingSource rentBindingSource;
        private clup_DBDataSetTableAdapters.rentTableAdapter rentTableAdapter;
        private System.Windows.Forms.BindingSource filmBindingSource;
        private clup_DBDataSetTableAdapters.filmTableAdapter filmTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.BindingSource seriesBindingSource;
        private clup_DBDataSetTableAdapters.SeriesTableAdapter seriesTableAdapter;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private clup_DBDataSetTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox usercode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox filmcode;
        private System.Windows.Forms.TextBox emtiaz;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox nazar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox code1;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriescodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesgenreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesSeasonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentpriceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesTotalPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesRengPartDataGridViewTextBoxColumn;
    }
}