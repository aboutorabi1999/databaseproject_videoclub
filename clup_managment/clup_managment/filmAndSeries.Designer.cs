﻿namespace clup_managment
{
    partial class filmAndSeries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.filmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clup_DBDataSet = new clup_managment.clup_DBDataSet();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.seriescodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesgenreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesSeasonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentpriceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesTotalPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesRengPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.seriesname = new System.Windows.Forms.TextBox();
            this.filmTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.filmTableAdapter();
            this.seriesTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.SeriesTableAdapter();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // filmBindingSource
            // 
            this.filmBindingSource.DataMember = "film";
            this.filmBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // clup_DBDataSet
            // 
            this.clup_DBDataSet.DataSetName = "clup_DBDataSet";
            this.clup_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.seriescodeDataGridViewTextBoxColumn,
            this.seriesnameDataGridViewTextBoxColumn,
            this.seriesdateDataGridViewTextBoxColumn,
            this.seriesgenreDataGridViewTextBoxColumn,
            this.seriesSeasonDataGridViewTextBoxColumn,
            this.seriesPartDataGridViewTextBoxColumn,
            this.rentpriceDataGridViewTextBoxColumn1,
            this.seriesTotalPartDataGridViewTextBoxColumn,
            this.seriesRengPartDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.seriesBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(12, 12);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(981, 446);
            this.dataGridView2.TabIndex = 1;
            // 
            // seriescodeDataGridViewTextBoxColumn
            // 
            this.seriescodeDataGridViewTextBoxColumn.DataPropertyName = "Series_code";
            this.seriescodeDataGridViewTextBoxColumn.HeaderText = "کد سریال";
            this.seriescodeDataGridViewTextBoxColumn.Name = "seriescodeDataGridViewTextBoxColumn";
            // 
            // seriesnameDataGridViewTextBoxColumn
            // 
            this.seriesnameDataGridViewTextBoxColumn.DataPropertyName = "Series_name";
            this.seriesnameDataGridViewTextBoxColumn.HeaderText = "نام سریال";
            this.seriesnameDataGridViewTextBoxColumn.Name = "seriesnameDataGridViewTextBoxColumn";
            // 
            // seriesdateDataGridViewTextBoxColumn
            // 
            this.seriesdateDataGridViewTextBoxColumn.DataPropertyName = "Series_date";
            this.seriesdateDataGridViewTextBoxColumn.HeaderText = "تاریخ انتشار";
            this.seriesdateDataGridViewTextBoxColumn.Name = "seriesdateDataGridViewTextBoxColumn";
            // 
            // seriesgenreDataGridViewTextBoxColumn
            // 
            this.seriesgenreDataGridViewTextBoxColumn.DataPropertyName = "Series_genre";
            this.seriesgenreDataGridViewTextBoxColumn.HeaderText = "ژانر";
            this.seriesgenreDataGridViewTextBoxColumn.Name = "seriesgenreDataGridViewTextBoxColumn";
            // 
            // seriesSeasonDataGridViewTextBoxColumn
            // 
            this.seriesSeasonDataGridViewTextBoxColumn.DataPropertyName = "Series_Season";
            this.seriesSeasonDataGridViewTextBoxColumn.HeaderText = "فصل";
            this.seriesSeasonDataGridViewTextBoxColumn.Name = "seriesSeasonDataGridViewTextBoxColumn";
            // 
            // seriesPartDataGridViewTextBoxColumn
            // 
            this.seriesPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Part";
            this.seriesPartDataGridViewTextBoxColumn.HeaderText = "قسمت";
            this.seriesPartDataGridViewTextBoxColumn.Name = "seriesPartDataGridViewTextBoxColumn";
            // 
            // rentpriceDataGridViewTextBoxColumn1
            // 
            this.rentpriceDataGridViewTextBoxColumn1.DataPropertyName = "rent_price";
            this.rentpriceDataGridViewTextBoxColumn1.HeaderText = "قیمت";
            this.rentpriceDataGridViewTextBoxColumn1.Name = "rentpriceDataGridViewTextBoxColumn1";
            // 
            // seriesTotalPartDataGridViewTextBoxColumn
            // 
            this.seriesTotalPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Total_Part";
            this.seriesTotalPartDataGridViewTextBoxColumn.HeaderText = "تعداد کل قسمت ها";
            this.seriesTotalPartDataGridViewTextBoxColumn.Name = "seriesTotalPartDataGridViewTextBoxColumn";
            // 
            // seriesRengPartDataGridViewTextBoxColumn
            // 
            this.seriesRengPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Reng_Part";
            this.seriesRengPartDataGridViewTextBoxColumn.HeaderText = "از قسمت ... تا قسمت ...";
            this.seriesRengPartDataGridViewTextBoxColumn.Name = "seriesRengPartDataGridViewTextBoxColumn";
            // 
            // seriesBindingSource
            // 
            this.seriesBindingSource.DataMember = "Series";
            this.seriesBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button2.Location = new System.Drawing.Point(432, 464);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(133, 42);
            this.button2.TabIndex = 7;
            this.button2.Text = "جستجو";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(859, 466);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 35);
            this.label2.TabIndex = 6;
            this.label2.Text = "نام فیلم یا  سریال";
            // 
            // seriesname
            // 
            this.seriesname.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_name", true));
            this.seriesname.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.seriesname.Location = new System.Drawing.Point(571, 466);
            this.seriesname.Name = "seriesname";
            this.seriesname.Size = new System.Drawing.Size(281, 40);
            this.seriesname.TabIndex = 5;
            // 
            // filmTableAdapter
            // 
            this.filmTableAdapter.ClearBeforeFill = true;
            // 
            // seriesTableAdapter
            // 
            this.seriesTableAdapter.ClearBeforeFill = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button4.Location = new System.Drawing.Point(293, 464);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 42);
            this.button4.TabIndex = 9;
            this.button4.Text = "حذف فیلتر";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button6.Location = new System.Drawing.Point(15, 464);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(133, 42);
            this.button6.TabIndex = 11;
            this.button6.Text = "جزئیات بیشتر";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(154, 464);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 42);
            this.button1.TabIndex = 12;
            this.button1.Text = "پخش فیلم";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // filmAndSeries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 578);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.seriesname);
            this.Controls.Add(this.dataGridView2);
            this.Name = "filmAndSeries";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "فیلم و سریال";
            this.Load += new System.EventHandler(this.filmAndSeries_Load);
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private clup_DBDataSet clup_DBDataSet;
        private System.Windows.Forms.BindingSource filmBindingSource;
        private clup_DBDataSetTableAdapters.filmTableAdapter filmTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource seriesBindingSource;
        private clup_DBDataSetTableAdapters.SeriesTableAdapter seriesTableAdapter;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox seriesname;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriescodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesgenreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesSeasonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentpriceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesTotalPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesRengPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}