﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class deleteFilmAndSeries : Form
    {
        public deleteFilmAndSeries()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (seriesname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

                try
                {
                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, '%' + seriesname.Text + '%');
                    filmTableAdapter.FillByname(clup_DBDataSet.film, '%' + seriesname.Text + '%');
                    

                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - ویدئو یافت نشد");
                }
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }

        private void deleteFilmAndSeries_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            // TODO: This line of code loads data into the 'clup_DBDataSet.rent' table. You can move, or remove it, as needed.
            this.rentTableAdapter.Fill(this.clup_DBDataSet.rent);
            // TODO: This line of code loads data into the 'clup_DBDataSet.characters' table. You can move, or remove it, as needed.
            this.charactersTableAdapter.Fill(this.clup_DBDataSet.characters);
            // TODO: This line of code loads data into the 'clup_DBDataSet.awards' table. You can move, or remove it, as needed.
            this.awardsTableAdapter.Fill(this.clup_DBDataSet.awards);
            // TODO: This line of code loads data into the 'clup_DBDataSet.Series' table. You can move, or remove it, as needed.
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            // TODO: This line of code loads data into the 'clup_DBDataSet.film' table. You can move, or remove it, as needed.
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int codee = 0;
            if (seriesname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

               try
                {

                DialogResult result;	//تعریف یک DialodResult به نام result
                result = MessageBox.Show("ب حذف فیلم کلیه اطلاعات اجاره ، افراد، و کارکترهای فیلم حذف میشود ادامه میدهید؟", "حذف!", MessageBoxButtons.YesNo);	
                if (result == DialogResult.Yes)
                {	

                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, seriesname.Text);
                    codee = int.Parse (textBox1.Text);
                    awardsTableAdapter.DeleteQuerybycode(codee);
                    charactersTableAdapter.DeleteQuerybycode(codee);
                    rentTableAdapter.DeleteQuerybyVideoCode(codee);
                    seriesTableAdapter.Deletebycode(codee);
                    filmTableAdapter.DeleteByCode(codee);
                    MessageBox.Show("با موفقیت حذف شد");
                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, seriesname.Text);
                }
              }
              catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود ویدئو یافت نشد");
                }
            }
        }
    }
}