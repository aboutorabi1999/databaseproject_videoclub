﻿namespace clup_managment
{
    partial class details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.videocodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awardsnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awardsDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awardscategoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awardsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clup_DBDataSet = new clup_managment.clup_DBDataSet();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.staffcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.videocodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charactersnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charactersKindDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charactersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.staffcodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffnameeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stafffamilyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffGenderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffNationalityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.rentcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.videocodeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usercodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateCommentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ratePriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.awardsTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.awardsTableAdapter();
            this.charactersTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.charactersTableAdapter();
            this.staffTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.staffTableAdapter();
            this.tableAdapterManager = new clup_managment.clup_DBDataSetTableAdapters.TableAdapterManager();
            this.rentTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.rentTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.awardsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charactersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.videocodeDataGridViewTextBoxColumn,
            this.awardsnameDataGridViewTextBoxColumn,
            this.awardsDateDataGridViewTextBoxColumn,
            this.awardscategoryDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.awardsBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(903, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // videocodeDataGridViewTextBoxColumn
            // 
            this.videocodeDataGridViewTextBoxColumn.DataPropertyName = "video_code";
            this.videocodeDataGridViewTextBoxColumn.HeaderText = "کد ویدئو";
            this.videocodeDataGridViewTextBoxColumn.Name = "videocodeDataGridViewTextBoxColumn";
            // 
            // awardsnameDataGridViewTextBoxColumn
            // 
            this.awardsnameDataGridViewTextBoxColumn.DataPropertyName = "awards_name";
            this.awardsnameDataGridViewTextBoxColumn.HeaderText = "نام جایزه";
            this.awardsnameDataGridViewTextBoxColumn.Name = "awardsnameDataGridViewTextBoxColumn";
            // 
            // awardsDateDataGridViewTextBoxColumn
            // 
            this.awardsDateDataGridViewTextBoxColumn.DataPropertyName = "awards_Date";
            this.awardsDateDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.awardsDateDataGridViewTextBoxColumn.Name = "awardsDateDataGridViewTextBoxColumn";
            // 
            // awardscategoryDataGridViewTextBoxColumn
            // 
            this.awardscategoryDataGridViewTextBoxColumn.DataPropertyName = "awards_category";
            this.awardscategoryDataGridViewTextBoxColumn.HeaderText = "جزئیات";
            this.awardscategoryDataGridViewTextBoxColumn.Name = "awardscategoryDataGridViewTextBoxColumn";
            // 
            // awardsBindingSource
            // 
            this.awardsBindingSource.DataMember = "awards";
            this.awardsBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // clup_DBDataSet
            // 
            this.clup_DBDataSet.DataSetName = "clup_DBDataSet";
            this.clup_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.staffcodeDataGridViewTextBoxColumn,
            this.videocodeDataGridViewTextBoxColumn1,
            this.charactersnameDataGridViewTextBoxColumn,
            this.charactersKindDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.charactersBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(12, 168);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(903, 150);
            this.dataGridView2.TabIndex = 1;
            // 
            // staffcodeDataGridViewTextBoxColumn
            // 
            this.staffcodeDataGridViewTextBoxColumn.DataPropertyName = "staff_code";
            this.staffcodeDataGridViewTextBoxColumn.HeaderText = "کد فرد";
            this.staffcodeDataGridViewTextBoxColumn.Name = "staffcodeDataGridViewTextBoxColumn";
            // 
            // videocodeDataGridViewTextBoxColumn1
            // 
            this.videocodeDataGridViewTextBoxColumn1.DataPropertyName = "video_code";
            this.videocodeDataGridViewTextBoxColumn1.HeaderText = "کد ویدئو";
            this.videocodeDataGridViewTextBoxColumn1.Name = "videocodeDataGridViewTextBoxColumn1";
            // 
            // charactersnameDataGridViewTextBoxColumn
            // 
            this.charactersnameDataGridViewTextBoxColumn.DataPropertyName = "characters_name";
            this.charactersnameDataGridViewTextBoxColumn.HeaderText = "نام شخصیت";
            this.charactersnameDataGridViewTextBoxColumn.Name = "charactersnameDataGridViewTextBoxColumn";
            // 
            // charactersKindDataGridViewTextBoxColumn
            // 
            this.charactersKindDataGridViewTextBoxColumn.DataPropertyName = "characters_Kind";
            this.charactersKindDataGridViewTextBoxColumn.HeaderText = "نوع شخصیت (بازیگر کارگردان و...)";
            this.charactersKindDataGridViewTextBoxColumn.Name = "charactersKindDataGridViewTextBoxColumn";
            this.charactersKindDataGridViewTextBoxColumn.Width = 300;
            // 
            // charactersBindingSource
            // 
            this.charactersBindingSource.DataMember = "characters";
            this.charactersBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.staffcodeDataGridViewTextBoxColumn1,
            this.staffnameeDataGridViewTextBoxColumn,
            this.stafffamilyDataGridViewTextBoxColumn,
            this.staffageDataGridViewTextBoxColumn,
            this.staffGenderDataGridViewTextBoxColumn,
            this.staffNationalityDataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.staffBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(12, 324);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(903, 150);
            this.dataGridView3.TabIndex = 2;
            // 
            // staffcodeDataGridViewTextBoxColumn1
            // 
            this.staffcodeDataGridViewTextBoxColumn1.DataPropertyName = "staff_code";
            this.staffcodeDataGridViewTextBoxColumn1.HeaderText = "کد فرد";
            this.staffcodeDataGridViewTextBoxColumn1.Name = "staffcodeDataGridViewTextBoxColumn1";
            // 
            // staffnameeDataGridViewTextBoxColumn
            // 
            this.staffnameeDataGridViewTextBoxColumn.DataPropertyName = "staff_namee";
            this.staffnameeDataGridViewTextBoxColumn.HeaderText = "نام";
            this.staffnameeDataGridViewTextBoxColumn.Name = "staffnameeDataGridViewTextBoxColumn";
            // 
            // stafffamilyDataGridViewTextBoxColumn
            // 
            this.stafffamilyDataGridViewTextBoxColumn.DataPropertyName = "staff_family";
            this.stafffamilyDataGridViewTextBoxColumn.HeaderText = "نام خانوادگی";
            this.stafffamilyDataGridViewTextBoxColumn.Name = "stafffamilyDataGridViewTextBoxColumn";
            // 
            // staffageDataGridViewTextBoxColumn
            // 
            this.staffageDataGridViewTextBoxColumn.DataPropertyName = "staff_age";
            this.staffageDataGridViewTextBoxColumn.HeaderText = "سن";
            this.staffageDataGridViewTextBoxColumn.Name = "staffageDataGridViewTextBoxColumn";
            // 
            // staffGenderDataGridViewTextBoxColumn
            // 
            this.staffGenderDataGridViewTextBoxColumn.DataPropertyName = "staff_Gender";
            this.staffGenderDataGridViewTextBoxColumn.HeaderText = "جنسیت";
            this.staffGenderDataGridViewTextBoxColumn.Name = "staffGenderDataGridViewTextBoxColumn";
            // 
            // staffNationalityDataGridViewTextBoxColumn
            // 
            this.staffNationalityDataGridViewTextBoxColumn.DataPropertyName = "staff_Nationality";
            this.staffNationalityDataGridViewTextBoxColumn.HeaderText = "ملیت";
            this.staffNationalityDataGridViewTextBoxColumn.Name = "staffNationalityDataGridViewTextBoxColumn";
            // 
            // staffBindingSource
            // 
            this.staffBindingSource.DataMember = "staff";
            this.staffBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("2  Homa", 9F);
            this.button1.Location = new System.Drawing.Point(12, 599);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(214, 32);
            this.button1.TabIndex = 15;
            this.button1.Text = "ثبت تغییرات";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("2  Homa", 9F);
            this.textBox1.Location = new System.Drawing.Point(232, 598);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 35);
            this.textBox1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("2  Homa", 9F);
            this.label1.Location = new System.Drawing.Point(338, 601);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 28);
            this.label1.TabIndex = 17;
            this.label1.Text = "میانگین امتیازات";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rentcodeDataGridViewTextBoxColumn,
            this.videocodeDataGridViewTextBoxColumn2,
            this.usercodeDataGridViewTextBoxColumn,
            this.rateDataGridViewTextBoxColumn,
            this.rateCommentDataGridViewTextBoxColumn,
            this.ratePriceDataGridViewTextBoxColumn});
            this.dataGridView4.DataSource = this.rentBindingSource;
            this.dataGridView4.Location = new System.Drawing.Point(12, 480);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView4.RowTemplate.Height = 24;
            this.dataGridView4.Size = new System.Drawing.Size(903, 113);
            this.dataGridView4.TabIndex = 18;
            // 
            // rentcodeDataGridViewTextBoxColumn
            // 
            this.rentcodeDataGridViewTextBoxColumn.DataPropertyName = "rent_code";
            this.rentcodeDataGridViewTextBoxColumn.HeaderText = "rent_code";
            this.rentcodeDataGridViewTextBoxColumn.Name = "rentcodeDataGridViewTextBoxColumn";
            this.rentcodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // videocodeDataGridViewTextBoxColumn2
            // 
            this.videocodeDataGridViewTextBoxColumn2.DataPropertyName = "video_code";
            this.videocodeDataGridViewTextBoxColumn2.HeaderText = "video_code";
            this.videocodeDataGridViewTextBoxColumn2.Name = "videocodeDataGridViewTextBoxColumn2";
            this.videocodeDataGridViewTextBoxColumn2.Visible = false;
            // 
            // usercodeDataGridViewTextBoxColumn
            // 
            this.usercodeDataGridViewTextBoxColumn.DataPropertyName = "User_code";
            this.usercodeDataGridViewTextBoxColumn.HeaderText = "کد کابر";
            this.usercodeDataGridViewTextBoxColumn.Name = "usercodeDataGridViewTextBoxColumn";
            // 
            // rateDataGridViewTextBoxColumn
            // 
            this.rateDataGridViewTextBoxColumn.DataPropertyName = "Rate";
            this.rateDataGridViewTextBoxColumn.HeaderText = "امتیاز";
            this.rateDataGridViewTextBoxColumn.Name = "rateDataGridViewTextBoxColumn";
            // 
            // rateCommentDataGridViewTextBoxColumn
            // 
            this.rateCommentDataGridViewTextBoxColumn.DataPropertyName = "Rate_Comment";
            this.rateCommentDataGridViewTextBoxColumn.HeaderText = "نظرات";
            this.rateCommentDataGridViewTextBoxColumn.Name = "rateCommentDataGridViewTextBoxColumn";
            // 
            // ratePriceDataGridViewTextBoxColumn
            // 
            this.ratePriceDataGridViewTextBoxColumn.DataPropertyName = "Rate_Price";
            this.ratePriceDataGridViewTextBoxColumn.HeaderText = "Rate_Price";
            this.ratePriceDataGridViewTextBoxColumn.Name = "ratePriceDataGridViewTextBoxColumn";
            this.ratePriceDataGridViewTextBoxColumn.Visible = false;
            // 
            // rentBindingSource
            // 
            this.rentBindingSource.DataMember = "rent";
            this.rentBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // awardsTableAdapter
            // 
            this.awardsTableAdapter.ClearBeforeFill = true;
            // 
            // charactersTableAdapter
            // 
            this.charactersTableAdapter.ClearBeforeFill = true;
            // 
            // staffTableAdapter
            // 
            this.staffTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.awardsTableAdapter = this.awardsTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.charactersTableAdapter = this.charactersTableAdapter;
            this.tableAdapterManager.filmTableAdapter = null;
            this.tableAdapterManager.rentTableAdapter = null;
            this.tableAdapterManager.SeriesTableAdapter = null;
            this.tableAdapterManager.staffTableAdapter = this.staffTableAdapter;
            this.tableAdapterManager.UpdateOrder = clup_managment.clup_DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UsersTableAdapter = null;
            // 
            // rentTableAdapter
            // 
            this.rentTableAdapter.ClearBeforeFill = true;
            // 
            // details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 643);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "details";
            this.RightToLeftLayout = true;
            this.Text = "جزئیات ویدئو";
            this.Load += new System.EventHandler(this.details_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.awardsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charactersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private clup_DBDataSet clup_DBDataSet;
        private System.Windows.Forms.BindingSource awardsBindingSource;
        private clup_DBDataSetTableAdapters.awardsTableAdapter awardsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn videocodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn awardsnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn awardsDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn awardscategoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource charactersBindingSource;
        private clup_DBDataSetTableAdapters.charactersTableAdapter charactersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn videocodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn charactersnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn charactersKindDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.BindingSource staffBindingSource;
        private clup_DBDataSetTableAdapters.staffTableAdapter staffTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffcodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffnameeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stafffamilyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffGenderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffNationalityDataGridViewTextBoxColumn;
        private clup_DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource rentBindingSource;
        private clup_DBDataSetTableAdapters.rentTableAdapter rentTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn videocodeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn usercodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateCommentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ratePriceDataGridViewTextBoxColumn;
    }
}