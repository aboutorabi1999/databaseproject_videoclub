﻿namespace clup_managment
{
    partial class user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.usercodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usernameeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userfamilyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userMailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userPassDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usertypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCreditDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clup_DBDataSet = new clup_managment.clup_DBDataSet();
            this.label1 = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.family = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pass = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.datee = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.usersTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.UsersTableAdapter();
            this.rentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rentTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.rentTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.usercodeDataGridViewTextBoxColumn,
            this.usernameeDataGridViewTextBoxColumn,
            this.userfamilyDataGridViewTextBoxColumn,
            this.userMailDataGridViewTextBoxColumn,
            this.userPassDataGridViewTextBoxColumn,
            this.usertypeDataGridViewTextBoxColumn,
            this.dateCreditDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.usersBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(851, 157);
            this.dataGridView1.TabIndex = 0;
            // 
            // usercodeDataGridViewTextBoxColumn
            // 
            this.usercodeDataGridViewTextBoxColumn.DataPropertyName = "User_code";
            this.usercodeDataGridViewTextBoxColumn.HeaderText = "کد کاربر";
            this.usercodeDataGridViewTextBoxColumn.Name = "usercodeDataGridViewTextBoxColumn";
            // 
            // usernameeDataGridViewTextBoxColumn
            // 
            this.usernameeDataGridViewTextBoxColumn.DataPropertyName = "User_namee";
            this.usernameeDataGridViewTextBoxColumn.HeaderText = "نام";
            this.usernameeDataGridViewTextBoxColumn.Name = "usernameeDataGridViewTextBoxColumn";
            // 
            // userfamilyDataGridViewTextBoxColumn
            // 
            this.userfamilyDataGridViewTextBoxColumn.DataPropertyName = "User_family";
            this.userfamilyDataGridViewTextBoxColumn.HeaderText = "نام خانوادگی";
            this.userfamilyDataGridViewTextBoxColumn.Name = "userfamilyDataGridViewTextBoxColumn";
            // 
            // userMailDataGridViewTextBoxColumn
            // 
            this.userMailDataGridViewTextBoxColumn.DataPropertyName = "User_Mail";
            this.userMailDataGridViewTextBoxColumn.HeaderText = "ایمیل";
            this.userMailDataGridViewTextBoxColumn.Name = "userMailDataGridViewTextBoxColumn";
            // 
            // userPassDataGridViewTextBoxColumn
            // 
            this.userPassDataGridViewTextBoxColumn.DataPropertyName = "User_Pass";
            this.userPassDataGridViewTextBoxColumn.HeaderText = "رمز";
            this.userPassDataGridViewTextBoxColumn.Name = "userPassDataGridViewTextBoxColumn";
            // 
            // usertypeDataGridViewTextBoxColumn
            // 
            this.usertypeDataGridViewTextBoxColumn.DataPropertyName = "User_type";
            this.usertypeDataGridViewTextBoxColumn.HeaderText = "نوع کاربری";
            this.usertypeDataGridViewTextBoxColumn.Name = "usertypeDataGridViewTextBoxColumn";
            // 
            // dateCreditDataGridViewTextBoxColumn
            // 
            this.dateCreditDataGridViewTextBoxColumn.DataPropertyName = "date_Credit";
            this.dateCreditDataGridViewTextBoxColumn.HeaderText = "تاریخ پایان عضویت";
            this.dateCreditDataGridViewTextBoxColumn.Name = "dateCreditDataGridViewTextBoxColumn";
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // clup_DBDataSet
            // 
            this.clup_DBDataSet.DataSetName = "clup_DBDataSet";
            this.clup_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(718, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 35);
            this.label1.TabIndex = 15;
            this.label1.Text = "کد کاربر";
            // 
            // code
            // 
            this.code.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_code", true));
            this.code.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.code.Location = new System.Drawing.Point(571, 178);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(140, 40);
            this.code.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(718, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 35);
            this.label2.TabIndex = 17;
            this.label2.Text = "نام";
            // 
            // name
            // 
            this.name.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_namee", true));
            this.name.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.name.Location = new System.Drawing.Point(571, 224);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(140, 40);
            this.name.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(718, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 35);
            this.label3.TabIndex = 19;
            this.label3.Text = "نام خانوادگی";
            // 
            // family
            // 
            this.family.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_family", true));
            this.family.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.family.Location = new System.Drawing.Point(571, 270);
            this.family.Name = "family";
            this.family.Size = new System.Drawing.Size(140, 40);
            this.family.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(718, 321);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 35);
            this.label4.TabIndex = 21;
            this.label4.Text = "ایمیل";
            // 
            // email
            // 
            this.email.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_Mail", true));
            this.email.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.email.Location = new System.Drawing.Point(571, 316);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(140, 40);
            this.email.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(718, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 35);
            this.label5.TabIndex = 23;
            this.label5.Text = "رمز";
            // 
            // pass
            // 
            this.pass.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_Pass", true));
            this.pass.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.pass.Location = new System.Drawing.Point(571, 362);
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(140, 40);
            this.pass.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(718, 413);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 35);
            this.label6.TabIndex = 25;
            this.label6.Text = "نوع کاربری";
            // 
            // type
            // 
            this.type.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "User_type", true));
            this.type.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.type.Location = new System.Drawing.Point(571, 408);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(140, 40);
            this.type.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(718, 459);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 35);
            this.label7.TabIndex = 27;
            this.label7.Text = "تاریخ پایان عضویت";
            // 
            // datee
            // 
            this.datee.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.usersBindingSource, "date_Credit", true));
            this.datee.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.datee.Location = new System.Drawing.Point(571, 454);
            this.datee.Name = "datee";
            this.datee.Size = new System.Drawing.Size(140, 40);
            this.datee.TabIndex = 26;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(432, 178);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 42);
            this.button1.TabIndex = 28;
            this.button1.Text = "جستجو";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button3.Location = new System.Drawing.Point(293, 178);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 42);
            this.button3.TabIndex = 29;
            this.button3.Text = "حذف فیلتر";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button5.Location = new System.Drawing.Point(154, 178);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(133, 42);
            this.button5.TabIndex = 30;
            this.button5.Text = "حذف";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button2.Location = new System.Drawing.Point(432, 404);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(133, 42);
            this.button2.TabIndex = 31;
            this.button2.Text = "ویرایش";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button4.Location = new System.Drawing.Point(432, 452);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 42);
            this.button4.TabIndex = 32;
            this.button4.Text = "افزودن";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // rentBindingSource
            // 
            this.rentBindingSource.DataMember = "rent";
            this.rentBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // rentTableAdapter
            // 
            this.rentTableAdapter.ClearBeforeFill = true;
            // 
            // user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 511);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.datee);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.type);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.email);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.family);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.code);
            this.Controls.Add(this.dataGridView1);
            this.Name = "user";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "اعضا و کاربران";
            this.Load += new System.EventHandler(this.user_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private clup_DBDataSet clup_DBDataSet;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private clup_DBDataSetTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn usercodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usernameeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userfamilyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userMailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userPassDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usertypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreditDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox family;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox pass;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox type;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox datee;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.BindingSource rentBindingSource;
        private clup_DBDataSetTableAdapters.rentTableAdapter rentTableAdapter;
    }
}