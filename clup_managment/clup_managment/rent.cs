﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class rent : Form
    {
        public rent()
        {
            InitializeComponent();
        }

        private void rent_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            Int32 num = 0;
            // TODO: This line of code loads data into the 'clup_DBDataSet.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter.FillBycode(this.clup_DBDataSet.Users, Class1.usercode);
            // TODO: This line of code loads data into the 'clup_DBDataSet.Series' table. You can move, or remove it, as needed.
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            // TODO: This line of code loads data into the 'clup_DBDataSet.film' table. You can move, or remove it, as needed.
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
            // TODO: This line of code loads data into the 'clup_DBDataSet.rent' table. You can move, or remove it, as needed.
            this.rentTableAdapter.Fill(this.clup_DBDataSet.rent);
            num = Convert.ToInt32(rentTableAdapter.ScalarQuerymaxRentCode());
            num++;
            code1.Text = num.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {

                try
                {
                    rentTableAdapter.Insert(int.Parse(code1.Text ),int.Parse(filmcode.Text ),int.Parse(usercode.Text ),int.Parse(emtiaz.Text ),nazar.Text,Price.Text );
                    MessageBox.Show(" با موفقیت ثبت شد");
                    this.rentTableAdapter.Fill(this.clup_DBDataSet.rent);

                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("Exception: " + ex.Message);
                    MessageBox.Show("خطا در ثبت ");
                }

        }
    }
}
