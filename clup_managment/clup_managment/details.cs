﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class details : Form
    {
        public details()
        {
            InitializeComponent();
        }

        private void details_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
// TODO: This line of code loads data into the 'clup_DBDataSet.rent' table. You can move, or remove it, as needed.
this.rentTableAdapter.Fill(this.clup_DBDataSet.rent);
            Int32 rate = 0;
            
            // TODO: This line of code loads data into the 'clup_DBDataSet.staff' table. You can move, or remove it, as needed.
            this.staffTableAdapter.Fill(this.clup_DBDataSet.staff);
            // TODO: This line of code loads data into the 'clup_DBDataSet.characters' table. You can move, or remove it, as needed.
            this.charactersTableAdapter.Fill(this.clup_DBDataSet.characters);
            // TODO: This line of code loads data into the 'clup_DBDataSet.awards' table. You can move, or remove it, as needed.
            this.awardsTableAdapter.Fill(this.clup_DBDataSet.awards);
            awardsTableAdapter.FillBycode(clup_DBDataSet.awards, int.Parse (Class1.videoCode.ToString()) );
            charactersTableAdapter.FillBycode(clup_DBDataSet.characters, int.Parse(Class1.videoCode.ToString()));
            staffTableAdapter.FillBycode(clup_DBDataSet.staff, int.Parse(Class1.videoCode.ToString()));
            rate = Convert.ToInt32(rentTableAdapter.ScalarQueryavgRateByVideoCode(int.Parse(Class1.videoCode.ToString())));
            textBox1.Text = rate.ToString();
            rentTableAdapter.FillByVideocode(clup_DBDataSet.rent , int.Parse(Class1.videoCode.ToString()));

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                staffTableAdapter.Update(clup_DBDataSet.staff);
                awardsTableAdapter.Update(clup_DBDataSet.awards);
                charactersTableAdapter.Update(clup_DBDataSet.characters);

                MessageBox.Show("تغییرات با موفقیت ثبت شد", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا در ثبت تغییرات", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
