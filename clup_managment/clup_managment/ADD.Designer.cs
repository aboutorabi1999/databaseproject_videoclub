﻿namespace clup_managment
{
    partial class ADDFilmAndSeries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.seriescodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesgenreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesSeasonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentpriceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesTotalPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesRengPartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clup_DBDataSet = new clup_managment.clup_DBDataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.scode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.sname = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.sganr = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.sdate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.snumOfFasl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.spart = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.sAllEpizode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sparttopart = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Sprice = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.filmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.filmTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.filmTableAdapter();
            this.seriesTableAdapter = new clup_managment.clup_DBDataSetTableAdapters.SeriesTableAdapter();
            this.button5 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.scode2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.seriescodeDataGridViewTextBoxColumn,
            this.seriesnameDataGridViewTextBoxColumn,
            this.seriesdateDataGridViewTextBoxColumn,
            this.seriesgenreDataGridViewTextBoxColumn,
            this.seriesSeasonDataGridViewTextBoxColumn,
            this.seriesPartDataGridViewTextBoxColumn,
            this.rentpriceDataGridViewTextBoxColumn1,
            this.seriesTotalPartDataGridViewTextBoxColumn,
            this.seriesRengPartDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.seriesBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.Location = new System.Drawing.Point(32, 12);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1091, 348);
            this.dataGridView2.TabIndex = 3;
            // 
            // seriescodeDataGridViewTextBoxColumn
            // 
            this.seriescodeDataGridViewTextBoxColumn.DataPropertyName = "Series_code";
            this.seriescodeDataGridViewTextBoxColumn.HeaderText = "کد ویدئو";
            this.seriescodeDataGridViewTextBoxColumn.Name = "seriescodeDataGridViewTextBoxColumn";
            // 
            // seriesnameDataGridViewTextBoxColumn
            // 
            this.seriesnameDataGridViewTextBoxColumn.DataPropertyName = "Series_name";
            this.seriesnameDataGridViewTextBoxColumn.HeaderText = "نام فیلم یا سریال";
            this.seriesnameDataGridViewTextBoxColumn.Name = "seriesnameDataGridViewTextBoxColumn";
            // 
            // seriesdateDataGridViewTextBoxColumn
            // 
            this.seriesdateDataGridViewTextBoxColumn.DataPropertyName = "Series_date";
            this.seriesdateDataGridViewTextBoxColumn.HeaderText = "تاریخ انتشار";
            this.seriesdateDataGridViewTextBoxColumn.Name = "seriesdateDataGridViewTextBoxColumn";
            // 
            // seriesgenreDataGridViewTextBoxColumn
            // 
            this.seriesgenreDataGridViewTextBoxColumn.DataPropertyName = "Series_genre";
            this.seriesgenreDataGridViewTextBoxColumn.HeaderText = "ژانر";
            this.seriesgenreDataGridViewTextBoxColumn.Name = "seriesgenreDataGridViewTextBoxColumn";
            // 
            // seriesSeasonDataGridViewTextBoxColumn
            // 
            this.seriesSeasonDataGridViewTextBoxColumn.DataPropertyName = "Series_Season";
            this.seriesSeasonDataGridViewTextBoxColumn.HeaderText = "فصل";
            this.seriesSeasonDataGridViewTextBoxColumn.Name = "seriesSeasonDataGridViewTextBoxColumn";
            // 
            // seriesPartDataGridViewTextBoxColumn
            // 
            this.seriesPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Part";
            this.seriesPartDataGridViewTextBoxColumn.HeaderText = "قسمت";
            this.seriesPartDataGridViewTextBoxColumn.Name = "seriesPartDataGridViewTextBoxColumn";
            // 
            // rentpriceDataGridViewTextBoxColumn1
            // 
            this.rentpriceDataGridViewTextBoxColumn1.DataPropertyName = "rent_price";
            this.rentpriceDataGridViewTextBoxColumn1.HeaderText = "قیمت";
            this.rentpriceDataGridViewTextBoxColumn1.Name = "rentpriceDataGridViewTextBoxColumn1";
            // 
            // seriesTotalPartDataGridViewTextBoxColumn
            // 
            this.seriesTotalPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Total_Part";
            this.seriesTotalPartDataGridViewTextBoxColumn.HeaderText = "تعداد کل قسمت ها";
            this.seriesTotalPartDataGridViewTextBoxColumn.Name = "seriesTotalPartDataGridViewTextBoxColumn";
            // 
            // seriesRengPartDataGridViewTextBoxColumn
            // 
            this.seriesRengPartDataGridViewTextBoxColumn.DataPropertyName = "Series_Reng_Part";
            this.seriesRengPartDataGridViewTextBoxColumn.HeaderText = "از قسمت ... تا قسمت ...";
            this.seriesRengPartDataGridViewTextBoxColumn.Name = "seriesRengPartDataGridViewTextBoxColumn";
            // 
            // seriesBindingSource
            // 
            this.seriesBindingSource.DataMember = "Series";
            this.seriesBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // clup_DBDataSet
            // 
            this.clup_DBDataSet.DataSetName = "clup_DBDataSet";
            this.clup_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(1017, 396);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 28);
            this.label6.TabIndex = 17;
            this.label6.Text = "کد ویدئو";
            // 
            // scode
            // 
            this.scode.Enabled = false;
            this.scode.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.scode.Location = new System.Drawing.Point(877, 396);
            this.scode.Name = "scode";
            this.scode.Size = new System.Drawing.Size(133, 35);
            this.scode.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(1017, 442);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 28);
            this.label7.TabIndex = 19;
            this.label7.Text = "نام فیلم یا سریال";
            // 
            // sname
            // 
            this.sname.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_name", true));
            this.sname.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sname.Location = new System.Drawing.Point(877, 442);
            this.sname.Name = "sname";
            this.sname.Size = new System.Drawing.Size(133, 35);
            this.sname.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(797, 444);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 28);
            this.label8.TabIndex = 23;
            this.label8.Text = "ژانر";
            // 
            // sganr
            // 
            this.sganr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_genre", true));
            this.sganr.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sganr.Location = new System.Drawing.Point(633, 444);
            this.sganr.Name = "sganr";
            this.sganr.Size = new System.Drawing.Size(157, 35);
            this.sganr.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.Location = new System.Drawing.Point(797, 398);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 28);
            this.label9.TabIndex = 21;
            this.label9.Text = "تاریخ انتشار";
            // 
            // sdate
            // 
            this.sdate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_date", true));
            this.sdate.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sdate.Location = new System.Drawing.Point(633, 398);
            this.sdate.Name = "sdate";
            this.sdate.Size = new System.Drawing.Size(157, 35);
            this.sdate.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.Location = new System.Drawing.Point(539, 399);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 28);
            this.label10.TabIndex = 25;
            this.label10.Text = "شماره فصل";
            // 
            // snumOfFasl
            // 
            this.snumOfFasl.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_Season", true));
            this.snumOfFasl.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.snumOfFasl.Location = new System.Drawing.Point(448, 399);
            this.snumOfFasl.Name = "snumOfFasl";
            this.snumOfFasl.Size = new System.Drawing.Size(84, 35);
            this.snumOfFasl.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label11.Location = new System.Drawing.Point(540, 444);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 28);
            this.label11.TabIndex = 27;
            this.label11.Text = "شماره قسمت";
            // 
            // spart
            // 
            this.spart.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_Part", true));
            this.spart.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.spart.Location = new System.Drawing.Point(449, 444);
            this.spart.Name = "spart";
            this.spart.Size = new System.Drawing.Size(84, 35);
            this.spart.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.Location = new System.Drawing.Point(324, 442);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 28);
            this.label12.TabIndex = 31;
            this.label12.Text = "کل قسمت ها";
            // 
            // sAllEpizode
            // 
            this.sAllEpizode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_Total_Part", true));
            this.sAllEpizode.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sAllEpizode.Location = new System.Drawing.Point(205, 442);
            this.sAllEpizode.Name = "sAllEpizode";
            this.sAllEpizode.Size = new System.Drawing.Size(112, 35);
            this.sAllEpizode.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(323, 397);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 28);
            this.label13.TabIndex = 29;
            this.label13.Text = "از قسمت تا قسمت";
            // 
            // sparttopart
            // 
            this.sparttopart.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_Reng_Part", true));
            this.sparttopart.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sparttopart.Location = new System.Drawing.Point(204, 397);
            this.sparttopart.Name = "sparttopart";
            this.sparttopart.Size = new System.Drawing.Size(112, 35);
            this.sparttopart.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.Location = new System.Drawing.Point(151, 395);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 28);
            this.label14.TabIndex = 33;
            this.label14.Text = "قیمت";
            // 
            // Sprice
            // 
            this.Sprice.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "rent_price", true));
            this.Sprice.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Sprice.Location = new System.Drawing.Point(32, 395);
            this.Sprice.Name = "Sprice";
            this.Sprice.Size = new System.Drawing.Size(112, 35);
            this.Sprice.TabIndex = 14;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button2.Location = new System.Drawing.Point(32, 437);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(167, 42);
            this.button2.TabIndex = 15;
            this.button2.Text = "افزودن";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(633, 485);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 42);
            this.button1.TabIndex = 34;
            this.button1.Text = "انتخاب فایل ویدئو";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(1017, 485);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 28);
            this.label1.TabIndex = 36;
            this.label1.Text = "آدرس ویدئو در رایانه";
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.filmBindingSource, "film_genre", true));
            this.textBox1.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox1.Location = new System.Drawing.Point(806, 485);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(204, 35);
            this.textBox1.TabIndex = 35;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button3.Location = new System.Drawing.Point(32, 527);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(167, 42);
            this.button3.TabIndex = 37;
            this.button3.Text = "ویرایش اطلاعات";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button4.Location = new System.Drawing.Point(32, 480);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(167, 42);
            this.button4.TabIndex = 38;
            this.button4.Text = "جستجو بر اساس نام";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // filmBindingSource
            // 
            this.filmBindingSource.DataMember = "film";
            this.filmBindingSource.DataSource = this.clup_DBDataSet;
            // 
            // filmTableAdapter
            // 
            this.filmTableAdapter.ClearBeforeFill = true;
            // 
            // seriesTableAdapter
            // 
            this.seriesTableAdapter.ClearBeforeFill = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("2  Homa", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button5.Location = new System.Drawing.Point(205, 485);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(111, 42);
            this.button5.TabIndex = 39;
            this.button5.Text = "حذف فیلتر";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(464, 492);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 28);
            this.label2.TabIndex = 41;
            this.label2.Text = "کد ویدئو";
            // 
            // scode2
            // 
            this.scode2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.seriesBindingSource, "Series_code", true));
            this.scode2.Enabled = false;
            this.scode2.Font = new System.Drawing.Font("2  Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.scode2.Location = new System.Drawing.Point(324, 492);
            this.scode2.Name = "scode2";
            this.scode2.Size = new System.Drawing.Size(133, 35);
            this.scode2.TabIndex = 40;
            // 
            // ADDFilmAndSeries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1141, 576);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.scode2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Sprice);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.sAllEpizode);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.sparttopart);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.spart);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.snumOfFasl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.sganr);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.sdate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.sname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.scode);
            this.Controls.Add(this.dataGridView2);
            this.Name = "ADDFilmAndSeries";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "افزودن فیلم یا سریال";
            this.Load += new System.EventHandler(this.ADDFilmAndSeries_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clup_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource seriesBindingSource;
        private clup_DBDataSet clup_DBDataSet;
        private System.Windows.Forms.BindingSource filmBindingSource;
        private clup_DBDataSetTableAdapters.filmTableAdapter filmTableAdapter;
        private clup_DBDataSetTableAdapters.SeriesTableAdapter seriesTableAdapter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox scode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox sname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox sganr;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox sdate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox snumOfFasl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox spart;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox sAllEpizode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox sparttopart;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Sprice;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriescodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesgenreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesSeasonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentpriceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesTotalPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seriesRengPartDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox scode2;

    }
}