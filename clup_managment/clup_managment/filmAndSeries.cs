﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace clup_managment
{
    public partial class filmAndSeries : Form
    {
        public filmAndSeries()
        {
            InitializeComponent();
        }

        private void filmAndSeries_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            if (Class1.userType=="0") 
            {
               
                dataGridView2.Enabled = true;
            }
            // TODO: This line of code loads data into the 'clup_DBDataSet.Series' table. You can move, or remove it, as needed.
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            // TODO: This line of code loads data into the 'clup_DBDataSet.Series' table. You can move, or remove it, as needed.
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            // TODO: This line of code loads data into the 'clup_DBDataSet.film' table. You can move, or remove it, as needed.
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.seriesTableAdapter.Fill(this.clup_DBDataSet.Series);
            this.filmTableAdapter.Fill(this.clup_DBDataSet.film);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (seriesname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

                try
                {
                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, '%' + seriesname.Text + '%');
                    filmTableAdapter.FillByname (clup_DBDataSet.film , '%' + seriesname.Text + '%');
                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - ویدئو یافت نشد");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
         
            
 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (seriesname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

                try
                {

                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, seriesname.Text);
                    Class1.videoCode=clup_DBDataSet.Series.Rows[0]["Series_code"].ToString();
                    Form details = new details();
                    details.ShowDialog();
                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - سریال یافت نشد");
                }
            }
       
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (seriesname.Text == "") { MessageBox.Show("لطفا نام  را وارد کنید"); }
            else
            {

                try
                {
                    seriesTableAdapter.FillByname(clup_DBDataSet.Series, '%' + seriesname.Text + '%');
                    filmTableAdapter.FillByname(clup_DBDataSet.film, '%' + seriesname.Text + '%');
                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - ویدئو یافت نشد");
                }
            }




             try
                {
            string filePath = "";
              filePath = clup_DBDataSet.film.Rows[0]["film_genre"].ToString();
              System.Diagnostics.Process.Start(@filePath.ToString());
            
                }
             catch (Exception ex)
             {
                 //display error message
                 MessageBox.Show("خطا در پخش");
             }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
