﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clup_managment
{
    public partial class user : Form
    {
        public user()
        {
            InitializeComponent();
        }

        private void user_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'clup_DBDataSet.rent' table. You can move, or remove it, as needed.
            this.rentTableAdapter.Fill(this.clup_DBDataSet.rent);
            this.CenterToScreen();
            // TODO: This line of code loads data into the 'clup_DBDataSet.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (code.Text == "") { MessageBox.Show("لطفا کد  را وارد کنید"); }
            else
            {

                try
                {

                    usersTableAdapter.FillBycode(clup_DBDataSet.Users, int.Parse(code.Text));

                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در جستجو - کاربر یافت نشد");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (code.Text == "") { MessageBox.Show("لطفا کد  را وارد کنید"); }
            else
            {

                try
                {
                    DialogResult result;	//تعریف یک DialodResult به نام result
                    result = MessageBox.Show("با حذف کاربر کلیه اطلاعات کرایه و نظرات فرد حذف میشود ادامه میدهید؟", "حذف!", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {

                        rentTableAdapter.DeleteQuerybyuserCode(int.Parse(code.Text));
                        usersTableAdapter.DeleteQuerybycode(int.Parse(code.Text));
                        MessageBox.Show("با موفقیت حذف شد");
                        this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.Message);
                    //display error message
                    MessageBox.Show("خطا در حذف - کاربر یافت نشد");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (code.Text == "") { MessageBox.Show("لطفا کد  را وارد کنید"); }
            else
            {

                try
                {

                    usersTableAdapter.Insert(int.Parse(code.Text), name.Text, family.Text, email.Text, pass.Text, int.Parse(type.Text), datee.Text);
                    MessageBox.Show("با موفقیت ثبت شد");

                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در افزودن - کاربر یافت نشد");
                    this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (code.Text == "") { MessageBox.Show("لطفا کد  را وارد کنید"); }
            else
            {

                try
                {

                    usersTableAdapter.UpdateQuerybycode(int.Parse(code.Text), name.Text, family.Text, email.Text, pass.Text, int.Parse(type.Text), datee.Text, int.Parse(code.Text));

                    MessageBox.Show("با موفقیت ویرایش شد");
                    this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);
                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ویرایش - کاربر یافت نشد");
                }
            }
        }
    }
}
