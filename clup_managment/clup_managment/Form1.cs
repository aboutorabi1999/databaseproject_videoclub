﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace clup_managment
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            // TODO: This line of code loads data into the 'clup_DBDataSet.Users' table. You can move, or remove it, as needed.
            this.usersTableAdapter.Fill(this.clup_DBDataSet.Users);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            
             if (code.Text == "" || pass.Text == "") { MessageBox.Show("لطفا اطلاعات  را وارد کنید"); }
            else
            {

                try
                {

                    usersTableAdapter.login(int.Parse(code.Text ),pass.Text );

                    Class1.usercode = int.Parse (code.Text);
                    Class1.userType = usersTableAdapter.ScalarQuerysearchUserTypeByCode(int.Parse(code.Text)).ToString ();
                    //MessageBox.Show(Class1.userType);
                     this.Close();

                }
                catch (Exception ex)
                {
                    //display error message
                    MessageBox.Show("خطا در ورود - کاربر یافت نشد");
                }
            }
        
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
         

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
