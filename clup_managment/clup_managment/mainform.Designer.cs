﻿namespace clup_managment
{
    partial class mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.b1 = new System.Windows.Forms.ToolStripMenuItem();
            this.لیستفیلمهاوسریالهاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.b2 = new System.Windows.Forms.ToolStripMenuItem();
            this.b3 = new System.Windows.Forms.ToolStripMenuItem();
            this.b4 = new System.Windows.Forms.ToolStripMenuItem();
            this.کاربرانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.u1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.menuStrip1.Font = new System.Drawing.Font("2  Homa", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.b1,
            this.کاربرانToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1007, 39);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // b1
            // 
            this.b1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.لیستفیلمهاوسریالهاToolStripMenuItem,
            this.b2,
            this.b3,
            this.b4});
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(139, 35);
            this.b1.Text = "فیلم ها و سریال ها";
            // 
            // لیستفیلمهاوسریالهاToolStripMenuItem
            // 
            this.لیستفیلمهاوسریالهاToolStripMenuItem.Name = "لیستفیلمهاوسریالهاToolStripMenuItem";
            this.لیستفیلمهاوسریالهاToolStripMenuItem.Size = new System.Drawing.Size(247, 36);
            this.لیستفیلمهاوسریالهاToolStripMenuItem.Text = "لیست فیلم ها و سریال ها";
            this.لیستفیلمهاوسریالهاToolStripMenuItem.Click += new System.EventHandler(this.لیستفیلمهاوسریالهاToolStripMenuItem_Click);
            // 
            // b2
            // 
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(247, 36);
            this.b2.Text = "افزودن فیلم و سریال";
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b3
            // 
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(247, 36);
            this.b3.Text = "حذف فیلم و سریال";
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // b4
            // 
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(247, 36);
            this.b4.Text = "کرایه یا فروش ";
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // کاربرانToolStripMenuItem
            // 
            this.کاربرانToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.u1});
            this.کاربرانToolStripMenuItem.Name = "کاربرانToolStripMenuItem";
            this.کاربرانToolStripMenuItem.Size = new System.Drawing.Size(66, 35);
            this.کاربرانToolStripMenuItem.Text = "کاربران";
            // 
            // u1
            // 
            this.u1.Name = "u1";
            this.u1.Size = new System.Drawing.Size(181, 36);
            this.u1.Text = "ثبت و ویرایش";
            this.u1.Click += new System.EventHandler(this.u1_Click);
            // 
            // mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::clup_managment.Properties.Resources.big_movie_reel_open_clapper_board_popcorn_box_vector_21649225_833x900;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1007, 538);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainform";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "مدیریت کلوپ فیلم و سریال";
            this.Load += new System.EventHandler(this.mainform_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem b1;
        private System.Windows.Forms.ToolStripMenuItem لیستفیلمهاوسریالهاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem b2;
        private System.Windows.Forms.ToolStripMenuItem b3;
        private System.Windows.Forms.ToolStripMenuItem b4;
        private System.Windows.Forms.ToolStripMenuItem کاربرانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem u1;

    }
}